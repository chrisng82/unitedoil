﻿using System;
using System.Windows.Forms;

namespace United_Oil___Autocount
{
    static class Program
    {
        //Create a variabel to store result
        public static DialogResult rslt;

        public static string _connectionString;

        //create a property to assign values to the rslt variable
        public static DialogResult rsltt
        {
            get
            {
                return rslt;
            }
            set
            {
                rslt = value;
            }
        }

        public static string connectionString
        {
            get
            {
                return _connectionString;
            }
            set
            {
                _connectionString = value;
            }
        }

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Login());


            //according to the rslt Open Form2 or Exit the application
            if (DialogResult.OK == rslt)
            {
                Application.Run(new MainForm(connectionString));
            }
            else
            {
                Application.Exit();
            }
        }
    }
}
