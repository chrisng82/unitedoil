﻿using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using Application = Microsoft.Office.Interop.Excel.Application;

namespace United_Oil___Autocount
{
    public partial class frmReportByTariff : Form
    {
        BindingSource bs = new BindingSource();
        private List<string> selectedTariffList = new List<string>();

        private List<DocTariff> docTariffList = new List<DocTariff>();

        private string _connectionString;

        public frmReportByTariff(string connectionString)
        {
            InitializeComponent();

            this._connectionString = connectionString;

            // To report progress from the background worker we need to set this property
            backgroundWorker1.WorkerReportsProgress = true;
            // This event will be raised on the worker thread when the worker starts
            backgroundWorker1.DoWork += new DoWorkEventHandler(backgroundWorker1_DoWork);
            // This event will be raised when we call ReportProgress
            backgroundWorker1.ProgressChanged += new ProgressChangedEventHandler(backgroundWorker1_ProgressChanged);
            backgroundWorker1.RunWorkerCompleted += BackgroundWorker1_RunWorkerCompleted;

            this.PopulateTariff();


        }

        private void BackgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            progressBar1.Hide();
            progressBar1.Visible = false;

            string message = "Report generated. Complete!";
            string title = "Complete";
            MessageBoxButtons buttons = MessageBoxButtons.OK;
            DialogResult result = MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);

            this.dateFrom.Enabled = true;
            this.dateTo.Enabled = true;
            this.checkedListTariff.Enabled = true;
            this.btnGenerate.Enabled = true;
            this.btnPreview.Enabled = true;
            this.tariffGridView.Enabled = true;
        }

        // On worker thread so do our thing!
        void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            string report_path = System.Configuration.ConfigurationManager.AppSettings["report_path"];

            if (report_path.Length == 0)
            {
                report_path = @"D:\UNITED OIL TARIFF REPORT";
            }

            // If directory does not exist, create it. 
            if (!Directory.Exists(report_path))
            {
                Directory.CreateDirectory(report_path);
            }

            this.CreateExcelDoc(report_path + "\\Report_" + DateTime.Now.ToString("dd_MM_yyyy_HH_mm_ss") + ".xlsx");
        }
        // Back on the 'UI' thread so we can update the progress bar
        void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            // The progress percentage is a property of e
            progressBar1.Value = e.ProgressPercentage;
            progressBar1.Refresh();
        }

        private void FrmReportByTariff_Load(object sender, EventArgs e)
        {

        }

        private void PopulateTariff()
        {
            using (SqlConnection cnn = new SqlConnection())
            {
                cnn.ConnectionString = this._connectionString;
                cnn.Open();

                string queryString = "SELECT DISTINCT TariffCode FROM [dbo].[Item] WHERE TariffCode <> '' " +
                    "ORDER BY TariffCode ASC";

                SqlCommand command = new SqlCommand(queryString, cnn);
                SqlDataReader reader = command.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        this.checkedListTariff.Items.Add(reader["TariffCode"], true);
                    }
                }
                finally
                {
                    // Always call Close when done reading.
                    reader.Close();
                }
            }
        }

        public void CreateExcelDoc(string fileName)
        {

            Application ExcelApp = new Application();
            Workbook ExcelWorkBook = null;
            Worksheet ExcelWorkSheet = null;
            ExcelApp.Visible = false;

            ExcelWorkBook = ExcelApp.Workbooks.Add(XlWBATemplate.xlWBATWorksheet);

            try
            {
                string prev_tariff_code = string.Empty;
                string curr_tariff_code = string.Empty;
                int noOfTariff = 0;

                int piLocalRow = 2, piImportRow = 2, asmRow = 2;

                int dProgress = 0; // ((double)e.BytesReceived / FileSize) * 100.0

                for (int c = 0; c < docTariffList.Count; c++)
                {
                    //dProgress = (int)(c / docTariffList.Count * 100);

                    dProgress = (int)Math.Round((double)c / (double)docTariffList.Count * 100);

                    backgroundWorker1.ReportProgress(dProgress);

                    DocTariff docTariff = docTariffList[c];
                    curr_tariff_code = docTariff.TariffCode;

                    // If new tariff code, add new sheet
                    if (string.Compare(prev_tariff_code, curr_tariff_code) != 0)
                    {
                        noOfTariff++;

                        if (noOfTariff == 1)
                        {
                            ExcelWorkSheet = ExcelWorkBook.Worksheets[1]; // Compulsory Line in which sheet you want to write data
                            ExcelWorkSheet.Name = curr_tariff_code;
                        }
                        else
                        {
                            // Keeping track
                            bool found = false;
                            // Loop through all worksheets in the workbook
                            foreach (Microsoft.Office.Interop.Excel.Worksheet sheet in ExcelWorkBook.Sheets)
                            {
                                // Check the name of the current sheet
                                if (sheet.Name == curr_tariff_code)
                                {
                                    found = true;
                                    break; // Exit the loop now
                                }
                            }

                            if (found)
                            {
                                // Reference it by name
                                ExcelWorkSheet = ExcelWorkBook.Sheets[curr_tariff_code];
                            }
                            else
                            {
                                ExcelWorkSheet = ExcelWorkBook.Sheets.Add(After: ExcelWorkBook.Sheets[ExcelWorkBook.Sheets.Count]);
                                ExcelWorkSheet.Name = curr_tariff_code;
                            }
                        }

                        // Reset row
                        piLocalRow = 2;
                        piImportRow = 2;
                        asmRow = 2;

                        // Write the header
                        int headerRow = 1, headerColumn = 1;

                        ExcelWorkSheet.Cells[headerRow, headerColumn++] = "Kod Tarif";
                        ExcelWorkSheet.Cells[headerRow, headerColumn++] = "Pembelian Tempatan";
                        ExcelWorkSheet.Cells[headerRow, headerColumn++] = "";
                        ExcelWorkSheet.Cells[headerRow, headerColumn++] = "";
                        ExcelWorkSheet.Cells[headerRow, headerColumn++] = "";
                        ExcelWorkSheet.Cells[headerRow, headerColumn++] = "";
                        ExcelWorkSheet.Cells[headerRow, headerColumn++] = "Pengimportan";
                        ExcelWorkSheet.Cells[headerRow, headerColumn++] = "";
                        ExcelWorkSheet.Cells[headerRow, headerColumn++] = "";
                        ExcelWorkSheet.Cells[headerRow, headerColumn++] = "";
                        ExcelWorkSheet.Cells[headerRow, headerColumn++] = "";
                        ExcelWorkSheet.Cells[headerRow, headerColumn++] = "Digunakan Dalam Pengilangan";
                        ExcelWorkSheet.Cells[headerRow, headerColumn++] = "";
                        ExcelWorkSheet.Cells[headerRow, headerColumn++] = "";
                        ExcelWorkSheet.Cells[headerRow, headerColumn++] = "";

                        headerRow++;
                        headerColumn = 1;

                        ExcelWorkSheet.Cells[headerRow, headerColumn++] = "";
                        ExcelWorkSheet.Cells[headerRow, headerColumn++] = "Nama Pembekal";
                        ExcelWorkSheet.Cells[headerRow, headerColumn++] = "Tarikh";
                        ExcelWorkSheet.Cells[headerRow, headerColumn++] = "No Invois";
                        ExcelWorkSheet.Cells[headerRow, headerColumn++] = "Kuantity";
                        ExcelWorkSheet.Cells[headerRow, headerColumn++] = "Nilai (RM)";
                        ExcelWorkSheet.Cells[headerRow, headerColumn++] = "Nama Pembekal";
                        ExcelWorkSheet.Cells[headerRow, headerColumn++] = "Tarikh";
                        ExcelWorkSheet.Cells[headerRow, headerColumn++] = "No Invois";
                        ExcelWorkSheet.Cells[headerRow, headerColumn++] = "Kuantity";
                        ExcelWorkSheet.Cells[headerRow, headerColumn++] = "Nilai (RM)";
                        ExcelWorkSheet.Cells[headerRow, headerColumn++] = "Tarikh";
                        ExcelWorkSheet.Cells[headerRow, headerColumn++] = "No Assembly";
                        ExcelWorkSheet.Cells[headerRow, headerColumn++] = "Kuantity";
                        ExcelWorkSheet.Cells[headerRow, headerColumn++] = "Nilai (RM)";
                    }

                    int rowToFill = 0;
                    int rowColumn = 1;
                    // If local Purchase Invoice
                    if (docTariff.DocType == "PI" && docTariff.CurrencyCode == "MYR")
                    {
                        piLocalRow++;
                        rowToFill = piLocalRow;

                        ExcelWorkSheet.Cells[rowToFill, 1] = docTariff.TariffCode;
                        ExcelWorkSheet.Cells[rowToFill, 2] = docTariff.CreditorName;
                        ExcelWorkSheet.Cells[rowToFill, 3] = "'" + docTariff.DocDate;
                        ExcelWorkSheet.Cells[rowToFill, 4] = docTariff.DocNo;
                        ExcelWorkSheet.Cells[rowToFill, 5] = docTariff.SmallestQty;
                        ExcelWorkSheet.Cells[rowToFill, 6] = docTariff.LocalTaxableAmt;
                    }
                    else if (docTariff.DocType == "PI" && docTariff.CurrencyCode != "MYR")
                    {
                        piImportRow++;
                        rowToFill = piImportRow;

                        ExcelWorkSheet.Cells[rowToFill, rowColumn++] = docTariff.TariffCode;
                        rowColumn++;
                        rowColumn++;
                        rowColumn++;
                        rowColumn++;
                        rowColumn++;
                        ExcelWorkSheet.Cells[rowToFill, rowColumn++] = docTariff.CreditorName;
                        ExcelWorkSheet.Cells[rowToFill, rowColumn++] = "'" + docTariff.DocDate;
                        ExcelWorkSheet.Cells[rowToFill, rowColumn++] = docTariff.DocNo;
                        ExcelWorkSheet.Cells[rowToFill, rowColumn++] = docTariff.SmallestQty;
                        ExcelWorkSheet.Cells[rowToFill, rowColumn++] = docTariff.LocalTaxableAmt;
                    }
                    else if (docTariff.DocType == "ASM")
                    {
                        asmRow++;
                        rowToFill = asmRow;

                        ExcelWorkSheet.Cells[rowToFill, rowColumn++] = docTariff.TariffCode;
                        rowColumn++;
                        rowColumn++;
                        rowColumn++;
                        rowColumn++;
                        rowColumn++;
                        rowColumn++;
                        rowColumn++;
                        rowColumn++;
                        rowColumn++;
                        rowColumn++;
                        ExcelWorkSheet.Cells[rowToFill, rowColumn++] = "'" + docTariff.DocDate;
                        ExcelWorkSheet.Cells[rowToFill, rowColumn++] = docTariff.DocNo;
                        ExcelWorkSheet.Cells[rowToFill, rowColumn++] = docTariff.SmallestQty;
                        ExcelWorkSheet.Cells[rowToFill, rowColumn++] = docTariff.LocalTaxableAmt;
                    }

                    prev_tariff_code = curr_tariff_code;

                }

                /*
                foreach (DocTariff docTariff in docTariffList)
                {
                    curr_tariff_code = docTariff.TariffCode;

                    // If new tariff code, add new sheet
                    if (string.Compare(prev_tariff_code, curr_tariff_code) != 0)
                    {
                        noOfTariff++;

                        if (noOfTariff == 1)
                        {
                            ExcelWorkSheet = ExcelWorkBook.Worksheets[1]; // Compulsory Line in which sheet you want to write data
                            ExcelWorkSheet.Name = curr_tariff_code;
                        }
                        else
                        {
                            // Keeping track
                            bool found = false;
                            // Loop through all worksheets in the workbook
                            foreach (Microsoft.Office.Interop.Excel.Worksheet sheet in ExcelWorkBook.Sheets)
                            {
                                // Check the name of the current sheet
                                if (sheet.Name == curr_tariff_code)
                                {
                                    found = true;
                                    break; // Exit the loop now
                                }
                            }

                            if (found)
                            {
                                // Reference it by name
                                ExcelWorkSheet = ExcelWorkBook.Sheets[curr_tariff_code];
                            }
                            else
                            {
                                ExcelWorkSheet = ExcelWorkBook.Sheets.Add(After: ExcelWorkBook.Sheets[ExcelWorkBook.Sheets.Count]);
                                ExcelWorkSheet.Name = curr_tariff_code;
                            }
                        }

                        // Reset row
                        piLocalRow = 2;
                        piImportRow = 2;
                        asmRow = 2;

                        // Write the header
                        int headerRow = 1, headerColumn = 1;

                        ExcelWorkSheet.Cells[headerRow, headerColumn++] = "Kod Tarif";
                        ExcelWorkSheet.Cells[headerRow, headerColumn++] = "Pembelian Tempatan";
                        ExcelWorkSheet.Cells[headerRow, headerColumn++] = "";
                        ExcelWorkSheet.Cells[headerRow, headerColumn++] = "";
                        ExcelWorkSheet.Cells[headerRow, headerColumn++] = "";
                        ExcelWorkSheet.Cells[headerRow, headerColumn++] = "";
                        ExcelWorkSheet.Cells[headerRow, headerColumn++] = "Pengimportan";
                        ExcelWorkSheet.Cells[headerRow, headerColumn++] = "";
                        ExcelWorkSheet.Cells[headerRow, headerColumn++] = "";
                        ExcelWorkSheet.Cells[headerRow, headerColumn++] = "";
                        ExcelWorkSheet.Cells[headerRow, headerColumn++] = "";
                        ExcelWorkSheet.Cells[headerRow, headerColumn++] = "Digunakan Dalam Pengilangan";
                        ExcelWorkSheet.Cells[headerRow, headerColumn++] = "";
                        ExcelWorkSheet.Cells[headerRow, headerColumn++] = "";
                        ExcelWorkSheet.Cells[headerRow, headerColumn++] = "";

                        headerRow++;
                        headerColumn = 1;

                        ExcelWorkSheet.Cells[headerRow, headerColumn++] = "";
                        ExcelWorkSheet.Cells[headerRow, headerColumn++] = "Nama Pembekal";
                        ExcelWorkSheet.Cells[headerRow, headerColumn++] = "Tarikh";
                        ExcelWorkSheet.Cells[headerRow, headerColumn++] = "No Invois";
                        ExcelWorkSheet.Cells[headerRow, headerColumn++] = "Kuantity";
                        ExcelWorkSheet.Cells[headerRow, headerColumn++] = "Nilai (RM)";
                        ExcelWorkSheet.Cells[headerRow, headerColumn++] = "Nama Pembekal";
                        ExcelWorkSheet.Cells[headerRow, headerColumn++] = "Tarikh";
                        ExcelWorkSheet.Cells[headerRow, headerColumn++] = "No Invois";
                        ExcelWorkSheet.Cells[headerRow, headerColumn++] = "Kuantity";
                        ExcelWorkSheet.Cells[headerRow, headerColumn++] = "Nilai (RM)";
                        ExcelWorkSheet.Cells[headerRow, headerColumn++] = "Tarikh";
                        ExcelWorkSheet.Cells[headerRow, headerColumn++] = "No Assembly";
                        ExcelWorkSheet.Cells[headerRow, headerColumn++] = "Kuantity";
                        ExcelWorkSheet.Cells[headerRow, headerColumn++] = "Nilai (RM)";
                    }

                    int rowToFill = 0;
                    int rowColumn = 1;
                    // If local Purchase Invoice
                    if (docTariff.DocType == "PI" && docTariff.CurrencyCode == "MYR")
                    {
                        piLocalRow++;
                        rowToFill = piLocalRow;

                        ExcelWorkSheet.Cells[rowToFill, 1] = docTariff.TariffCode;
                        ExcelWorkSheet.Cells[rowToFill, 2] = docTariff.CreditorName;
                        ExcelWorkSheet.Cells[rowToFill, 3] = "'" + docTariff.DocDate;
                        ExcelWorkSheet.Cells[rowToFill, 4] = docTariff.DocNo;
                        ExcelWorkSheet.Cells[rowToFill, 5] = docTariff.SmallestQty;
                        ExcelWorkSheet.Cells[rowToFill, 6] = docTariff.LocalTaxableAmt;
                    }
                    else if (docTariff.DocType == "PI" && docTariff.CurrencyCode != "MYR")
                    {
                        piImportRow++;
                        rowToFill = piImportRow;

                        ExcelWorkSheet.Cells[rowToFill, rowColumn++] = docTariff.TariffCode;
                        rowColumn++;
                        rowColumn++;
                        rowColumn++;
                        rowColumn++;
                        rowColumn++;
                        ExcelWorkSheet.Cells[rowToFill, rowColumn++] = docTariff.CreditorName;
                        ExcelWorkSheet.Cells[rowToFill, rowColumn++] = "'" + docTariff.DocDate;
                        ExcelWorkSheet.Cells[rowToFill, rowColumn++] = docTariff.DocNo;
                        ExcelWorkSheet.Cells[rowToFill, rowColumn++] = docTariff.SmallestQty;
                        ExcelWorkSheet.Cells[rowToFill, rowColumn++] = docTariff.LocalTaxableAmt;
                    }
                    else if (docTariff.DocType == "ASM")
                    {
                        asmRow++;
                        rowToFill = asmRow;

                        ExcelWorkSheet.Cells[rowToFill, rowColumn++] = docTariff.TariffCode;
                        rowColumn++;
                        rowColumn++;
                        rowColumn++;
                        rowColumn++;
                        rowColumn++;
                        rowColumn++;
                        rowColumn++;
                        rowColumn++;
                        rowColumn++;
                        rowColumn++;
                        ExcelWorkSheet.Cells[rowToFill, rowColumn++] = "'" + docTariff.DocDate;
                        ExcelWorkSheet.Cells[rowToFill, rowColumn++] = docTariff.DocNo;
                        ExcelWorkSheet.Cells[rowToFill, rowColumn++] = docTariff.SmallestQty;
                        ExcelWorkSheet.Cells[rowToFill, rowColumn++] = docTariff.LocalTaxableAmt;
                    }
                    
                    prev_tariff_code = curr_tariff_code;
                }
                */

                ExcelWorkBook.SaveAs(fileName);
                ExcelWorkBook.Close();
                ExcelApp.Quit();

                Marshal.ReleaseComObject(ExcelWorkSheet);
                Marshal.ReleaseComObject(ExcelWorkBook);
                Marshal.ReleaseComObject(ExcelApp);


            }

            catch (Exception exHandle)
            {
                Console.WriteLine("Exception: " + exHandle.Message);
                Console.ReadLine();
            }
            finally
            {
                foreach (Process process in Process.GetProcessesByName("Excel"))
                    process.Kill();
            }

        }

        private void BtnGenerate_Click(object sender, EventArgs e)
        {
            this.dateFrom.Enabled = false;
            this.dateTo.Enabled = false;
            this.checkedListTariff.Enabled = false;
            this.btnGenerate.Enabled = false;
            this.btnPreview.Enabled = false;
            this.tariffGridView.Enabled = false;

            progressBar1.Visible = true;
            progressBar1.Value = 0;
            progressBar1.Show();

            backgroundWorker1.RunWorkerAsync();
        }

        private void BtnPreview_Click(object sender, EventArgs e)
        {
            docTariffList.Clear();
            bs = new BindingSource();
            selectedTariffList.Clear();

            foreach (object itemChecked in checkedListTariff.CheckedItems)
            {
                selectedTariffList.Add(itemChecked.ToString());
            }

            string startDate = dateFrom.Value.ToString("yyyy-MM-dd");
            string endDate = dateTo.Value.ToString("yyyy-MM-dd");

            string tariff_criteria = " i.TariffCode IN (" + String.Join(", ", selectedTariffList.ToArray()) + ")";

            string queryString = "SELECT 'PI' AS DOC_TYPE, convert(varchar, h.DocDate, 105) AS DOC_DATE, h.DocNo AS DOC_NO, h.CreditorName AS CREDITOR_NAME, " +
                "h.CurrencyCode AS CURRENCY, " +
                "h.CurrencyRate AS CURRENCY_RATE, i.TariffCode AS TARIFF_CODE, SUM(d.SmallestQty) AS TOTAL_QTY, " +
                "SUM(d.LocalTaxableAmt) AS TOTAL_VALUE " +
                "FROM PIDTL d LEFT JOIN ITEM i ON i.ItemCode = d.ItemCode " +
                "LEFT JOIN PI h ON h.DocKey = d.DocKey WHERE " + tariff_criteria + " AND h.DocDate >= '" + startDate + "' AND h.DocDate <= '" + endDate + "' " +
                "GROUP BY i.TariffCode, h.CreditorName, h.DocDate, h.DocNo, h.CurrencyCode, h.CurrencyRate " +
                "ORDER BY i.TariffCode, h.DocDate, h.DocNo ";


            string queryString2 = "SELECT 'ASM' AS DOC_TYPE, convert(varchar, h.DocDate, 105) AS DOC_DATE, h.DocNo AS DOC_NO, '' AS CREDITOR_NAME, " +
                "'MYR' AS CURRENCY, " +
                "1 AS CURRENCY_RATE, i.TariffCode AS TARIFF_CODE, SUM(d.QTY) AS TOTAL_QTY, " +
                "SUM(d.SubTotalCost) AS TOTAL_VALUE " +
                "FROM ASMDTL d LEFT JOIN ITEM i ON i.ItemCode = d.ItemCode " +
                "LEFT JOIN ASM h ON h.DocKey = d.DocKey WHERE i.TariffCode <> '' AND h.DocDate >= '" + startDate + "' AND h.DocDate <= '" + endDate + "' " +
                "GROUP BY i.TariffCode, h.DocDate, h.DocNo " +
                "ORDER BY i.TariffCode, h.DocDate ASC, h.DocNo DESC ";

            using (SqlConnection connection = new SqlConnection(this._connectionString))
            {
                SqlCommand command = new SqlCommand(queryString, connection);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();

                try
                {
                    while (reader.Read())
                    {
                        DocTariff docTariff = new DocTariff();
                        docTariff.DocType = reader["DOC_TYPE"].ToString();
                        docTariff.DocNo = reader["DOC_NO"].ToString();
                        docTariff.DocDate = reader["DOC_DATE"].ToString();
                        docTariff.CreditorName = reader["CREDITOR_NAME"].ToString();
                        docTariff.CurrencyCode = reader["CURRENCY"].ToString();
                        docTariff.CurrencyRate = Double.Parse(reader["CURRENCY_RATE"].ToString());
                        docTariff.TariffCode = reader["TARIFF_CODE"].ToString();
                        docTariff.SmallestQty = Convert.ToInt32(reader["TOTAL_QTY"]);
                        docTariff.LocalTaxableAmt = (decimal)reader["TOTAL_VALUE"];

                        docTariffList.Add(docTariff);
                    }
                }
                finally
                {
                    // Always call Close when done reading.
                    reader.Close();
                }

                SqlCommand command2 = new SqlCommand(queryString2, connection);
                SqlDataReader reader2 = command2.ExecuteReader();
                try
                {
                    while (reader2.Read())
                    {
                        DocTariff docTariff = new DocTariff();
                        docTariff.DocType = reader2["DOC_TYPE"].ToString();
                        docTariff.DocNo = reader2["DOC_NO"].ToString();
                        docTariff.DocDate = reader2["DOC_DATE"].ToString();
                        docTariff.CurrencyCode = reader2["CURRENCY"].ToString();
                        docTariff.CurrencyRate = Double.Parse(reader2["CURRENCY_RATE"].ToString());
                        docTariff.TariffCode = reader2["TARIFF_CODE"].ToString();
                        docTariff.SmallestQty = Convert.ToInt32(reader2["TOTAL_QTY"]);
                        docTariff.LocalTaxableAmt = (decimal)reader2["TOTAL_VALUE"];

                        docTariffList.Add(docTariff);
                    }
                }
                finally
                {
                    // Always call Close when done reading.
                    reader2.Close();
                }
            }

            bs.DataSource = docTariffList;
            tariffGridView.DataSource = bs;
            tariffGridView.Update();
            tariffGridView.Refresh();
        }
    }
}
