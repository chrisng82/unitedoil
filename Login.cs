﻿using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace United_Oil___Autocount
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        private void BtnConnect_Click(object sender, EventArgs e)
        {
            string connetionString;
            string servername = this.txtDataSource.Text;
            string databasename = this.txtDatabaseName.Text;
            string reportpath = this.txtPath.Text;

            string username = System.Configuration.ConfigurationManager.AppSettings["username"];
            string password = System.Configuration.ConfigurationManager.AppSettings["password"];

            connetionString = @"Data Source=" + servername + "; Initial Catalog=" + databasename + ";User ID="
                + username + ";Password=" + password;

            try
            {

                using (SqlConnection cnn = new SqlConnection())
                {
                    cnn.ConnectionString = connetionString;
                    cnn.Open();
                }

                //tells the Program class I'm successfull to show form2
                Program.rsltt = DialogResult.OK;

                Program.connectionString = connetionString;

                System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

                config.AppSettings.Settings["server"].Value = servername;
                config.AppSettings.Settings["database"].Value = databasename;
                config.AppSettings.Settings["report_path"].Value = reportpath;

                config.Save(ConfigurationSaveMode.Modified);

                MainForm f1 = new MainForm(connetionString);
                f1.Show();

                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Cannot connect to Autocount Database. Please check the settings." +
                    "\n" + ex.Message + "\n" + ex.StackTrace, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Login_Load(object sender, EventArgs e)
        {
            string server = System.Configuration.ConfigurationManager.AppSettings["server"];
            string database = System.Configuration.ConfigurationManager.AppSettings["database"];
            string report_path = System.Configuration.ConfigurationManager.AppSettings["report_path"];

            this.txtDataSource.Text = server;
            this.txtDatabaseName.Text = database;
            this.txtPath.Text = report_path;
        }
    }
}
