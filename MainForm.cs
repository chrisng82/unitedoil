﻿using System;
using System.Windows.Forms;

namespace United_Oil___Autocount
{
    public partial class MainForm : Form
    {
        private string connectionString;

        public MainForm(string _connectionString)
        {
            InitializeComponent();

            this.connectionString = _connectionString;
        }

        private void ReportByTariffCodeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmReportByTariff frmReportByTariff = new frmReportByTariff(connectionString);
            frmReportByTariff.MdiParent = this;
            frmReportByTariff.Show();
        }
    }
}
