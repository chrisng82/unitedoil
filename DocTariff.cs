﻿using System.ComponentModel;

namespace United_Oil___Autocount
{
    public class DocTariff
    {
        private string docno;
        private string doctype;
        private string docdate;
        private string creditorname;
        private string currencycode;
        private string tariffcode;
        private double currencyrate;
        private int smallestqty;
        private decimal localtaxableamt;


        [DisplayName("Tariff Code")]
        public string TariffCode
        {
            get { return tariffcode; }

            set { tariffcode = value; }
        }


        [DisplayName("Doc Type")]

        public string DocType
        {
            get { return doctype; }

            set { doctype = value; }
        }

        [DisplayName("Doc No")]
        public string DocNo
        {
            get { return docno; }

            set { docno = value; }
        }

        [DisplayName("Doc Date")]
        public string DocDate
        {
            get { return docdate; }

            set { docdate = value; }
        }

        [DisplayName("Supplier")]
        public string CreditorName
        {
            get { return creditorname; }

            set { creditorname = value; }
        }



        [DisplayName("Currency")]
        public string CurrencyCode
        {
            get { return currencycode; }

            set { currencycode = value; }
        }

        [DisplayName("Currency Rate")]
        public double CurrencyRate
        {
            get { return currencyrate; }

            set { currencyrate = value; }

        }

        [DisplayName("Total Quantity")]
        public int SmallestQty
        {
            get { return smallestqty; }

            set { smallestqty = value; }
        }

        [DisplayName("Total Value")]
        public decimal LocalTaxableAmt
        {
            get { return localtaxableamt; }

            set { localtaxableamt = value; }
        }





    }
}
